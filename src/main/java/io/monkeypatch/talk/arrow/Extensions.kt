package io.monkeypatch.talk.arrow

class Greeter(val prefix: String) {
    fun String.greet() = println("$prefix $this")
}

fun main(args: Array<String>) {
    with(Greeter("Hello")) {
        "Alex".greet()
        "Guillaume".greet()
    }

    Greeter("Ook ook 🍌").run {
        "Alex".greet()
        "Guillaume".greet()
    }
}