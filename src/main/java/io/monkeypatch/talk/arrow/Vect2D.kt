package io.monkeypatch.talk.arrow

data class Vect2D(val x: Double, val y: Double) {
    operator fun plus(other: Vect2D) =
        Vect2D(x + other.x, y + other.y)
    companion object
}