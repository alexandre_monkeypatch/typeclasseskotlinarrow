package io.monkeypatch.talk.arrow

import arrow.instance
import arrow.instances.monoid
import arrow.typeclasses.Monoid

inline fun <A> foldN(n: Int, seed: A, operation: (A) -> A): A {
    var acc = seed
    for (i in 1..n) {
        acc = operation(acc)
    }
    return acc
}

fun <A> Monoid<A>.naturalMultiply(a: A, n: Int) =
    foldN(n, empty()) { it.combine(a) }

fun <A, B> pairMonoid(monoidA: Monoid<A>, monoidB: Monoid<B>): Monoid<Pair<A, B>> = object : Monoid<Pair<A, B>> {
    override fun empty(): Pair<A, B> = monoidA.empty() to monoidB.empty()

    override fun Pair<A, B>.combine(b: Pair<A, B>): Pair<A, B> =
        with(monoidA) { first.combine(b.first) } to
                with(monoidB) { second.combine(b.second) }
}

@instance(Vect2D::class)
interface Vect2DMonoid : Monoid<Vect2D> {
    override fun empty(): Vect2D = Vect2D(0.0, 0.0)

    override fun Vect2D.combine(b: Vect2D): Vect2D =
        Vect2D(x + b.x, y + b.y)
}

fun main(args: Array<String>) {
    with(Int.monoid()) {
        println(naturalMultiply(42, 10))
    }

    with(Double.monoid()) {
        println(naturalMultiply(42.1, 10))
    }

    with(String.monoid()) {
        println(naturalMultiply("🍌", 10))
    }

    with(pairMonoid(Int.monoid(), String.monoid())) {
        println(naturalMultiply(42 to "🍌", 10))
    }

    with(Vect2D.monoid()) {
        println(naturalMultiply(Vect2D(1.0, 2.0), 10))
    }
}
